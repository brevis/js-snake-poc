const app = document.getElementById('app');

const ROWS = 20;
const COLS = 20;
const UP = 'UP';
const DOWN = 'DOWN';
const LEFT = 'LEFT';
const RIGHT = 'RIGHT';
const HEAD = 1;
const BODY = 2;
const APPLE = -1;
const REFRESH_TIME = 100;
const STOP = 1;
const RUN = 2;

let snakeBody = [];
let board = [];
let boardRef = [];
let snakeLength = 0;
let snakeHead = {}
let snakeTail = {};
let headDirection = 'UP';
let tailDirection = 'UP';
let inputEventAttached = false;
let gameState = STOP;

const makeBoard = () => {
    // generate html
    let html = '<table>';
    for (let row = 0; row < ROWS; row++) {
        html += '<tr>';
        let boardRow = [];
        for (let col = 0; col < COLS; col++) {
            let cellId = 'c' + row + '_' + col;
            html += '<td id="' + cellId + '"></td>';
            boardRow.push(0);
        }    
        html += '</tr>';
        board.push(boardRow);
    }
    html += '</table>';
    app.innerHTML = html;

    // save refs
    for (let row = 0; row < ROWS; row++) {
        let refRow = [];
        for (let col = 0; col < COLS; col++) {
            let cellId = 'c' + row + '_' + col;     
            let el = document.getElementById(cellId);
            refRow.push(el);
        }    
        boardRef.push(refRow);
    }

    gameState = RUN;
}

const renderBoard = () => {
    for (let row = 0; row < ROWS; row++) {
        for (let col = 0; col < COLS; col++) {
            let className = '';
            let cellValue = board[row][col];
            if (cellValue == HEAD) className = 'head';
            if (cellValue == BODY) className = 'body';
            if (cellValue == APPLE) className = 'apple';
            boardRef[row][col].className = className;
        }    
    }
}

const updateGame = () => {
    // 1. process input
    processInput();

    // 2. moving: 
    //  1) move HEAD forward
    //  2) delete TAIL
    moveForward();

    // 3. eating:
    //  growSnake()
    
    if (gameState === RUN) {
        renderBoard();
        setTimeout(updateGame, REFRESH_TIME);
    }
}

const processInput = () => {
    if (!inputEventAttached) {
        document.body.addEventListener('keydown', (e) => {
            switch (e.code) {
                case 'ArrowUp':
                    if (headDirection !== DOWN) {
                        headDirection = UP;
                    }
                    break;
                case 'ArrowDown':
                    if (headDirection !== UP) {
                        headDirection = DOWN;
                    }                    
                    break;
                case 'ArrowLeft':
                    if (headDirection !== RIGHT) {
                        headDirection = LEFT;
                    }
                    break;
                case 'ArrowRight':
                    if (headDirection !== LEFT) {
                        headDirection = RIGHT;
                    }
                    break;
            }
        });
        inputEventAttached = true;
    }
}

const moveForward = () => {
    let newRowHead = snakeHead.row;
    let newColHead = snakeHead.col;   
    switch (headDirection) {
        case UP:
            newRowHead--;
            if (newRowHead < 0) newRowHead = ROWS - 1;
            break;
        case DOWN:
            newRowHead++;
            if (newRowHead >= ROWS) newRowHead = 0;
            break;            
        case LEFT:
            newColHead--;
            if (newColHead < 0) newColHead = COLS - 1;
            break;
        case RIGHT:
            newColHead++;
            if (newColHead > COLS) newColHead = 0;            
            break;
    }
    
    if (isGameOver(newRowHead, newColHead)) {
        gameState = STOP;
        alert('Game over');
    }

    let isEating = board[newRowHead][newColHead] == APPLE;
    board[newRowHead][newColHead] = HEAD;    
    let newSnakeHead = getSnakeCell(newRowHead, newColHead, null, snakeHead);
    snakeHead.prev = newSnakeHead;
    snakeHead = newSnakeHead;

    if (isEating) {
        growSnake();
        placeApple();
    }

    if (snakeLength > 1) {
        board[snakeHead.next.row][snakeHead.next.col] = BODY;
    }

    board[snakeTail.row][snakeTail.col] = 0;
    snakeTail = snakeTail.prev;    
}

const growSnake = () => {
    if (snakeLength == 0) {
        board[10][10] = HEAD;
        snakeHead = getSnakeCell(10, 10, null, null);
        snakeTail = snakeHead;
    } else {
        board[snakeTail.row][snakeTail.col] = BODY;
        snakeTail = getSnakeCell(snakeTail.row, snakeTail.col, snakeTail);
    }
    snakeLength++;
}

const placeApple = () => {
    let freeCells = [];
    for (let row  = 0; row < ROWS; row++) {
        for (let col = 0; col < COLS; col++) {
            if (board[row][col] == 0) {
                freeCells.push([row, col]);
            }
        }
    }

    if (freeCells.length == 0) {
        alert('Game over');
        gameState = STOP;
        return;
    }

    let appleCell = freeCells[rnd(0, freeCells.length)];
    board[appleCell[0]][appleCell[1]] = APPLE;
}

const isGameOver = (row, col) => {
    return [BODY, HEAD].indexOf(board[row][col]) !== -1;
}

const getSnakeCell = (row, col, prev, next) => {
    return {row, col, prev, next};
}

const rnd = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

// loop

makeBoard();

growSnake();
placeApple();

updateGame();